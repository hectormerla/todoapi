<?php

class TaskController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$tasks = Task::all();
		return $tasks;
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$rules = array(
			'title'	=> 'required'
		);
		$validator = Validator::make( Input::all(), $rules );

		if( $validator->fails() ){
			return false;
		} else {

			$task = new Task;
			$task->title 	= Input::get('title');
			$task->desc 	= Input::get('description');
			$task->save();

			return true;
		}
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		// show just one task
		$task = Task::find($id);

		return $task;
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		// 
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		// update task
		$rules = array(
			'title'	=> 'required'
		);
		$validator = Validator::make( Input::all(), $rules );

		if( $validator->fails() ){
			return false;
		} else {

			$task = find($id);
			$task->title 	= Input::get('title');
			$task->desc 	= Input::get('description');
			$task->save();

			return true;
		}
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}


}
